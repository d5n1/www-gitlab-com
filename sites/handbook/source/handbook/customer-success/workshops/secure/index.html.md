---
layout: handbook-page-toc
title: "Secure Workshop"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The Secure Workshop is a Customer Success activity to provide customer enablement on [Secure stage features](https://about.gitlab.com/stages-devops-lifecycle/secure/).

The objectives of a Secure Workshop are to:

- Provide the customer with best practices for Secure capabilities
- Help the customer to adopt Secure stage features they already have access to
- Show the customer the value of the Secure stage to position tier upgrade

A Secure Workshop is **not** a hands-on-keyboard demo or implementation effort.

## Getting Started

When first discussing a Secure Workshop with your customer, you can use these points to describe the purpose of the workshop, and set expectations.

- The Secure Workshop is tailored to your interest in Secure capabilities.
- We will work with you to determine which information to cover, and the objectives you are looking to achieve with Secure.
- We will work with you to determine when to schedule the workshop, and will provide information to send to the attendees you identify on how to sign up to participate, and how the workshop will work.
- Your TAM, in conjunction with a team from GitLab, will deliver the workshop and answer questions.
- Depending on the number of topics to be covered the workshop length will vary, but you should plan for a minimum of 90 minutes.

## Planning

Once a customer has agreed to conduct a Secure Workshop session, we need to scope the workshop and what topics should be covered.

### Discovery Questions

To determine the scope and contents of the workshop, refer to the [discovery questionnaire](questionnaire/) which provides guidance on information to gather and questions to ask, as well as links to additional questions and resources to use.

### Workshop Development

Use the information you attained during discovery to plan how you will deliver the workshop, and collaborate with the customer to make sure you're covering the right information. Use the [provided materials](#workshop-materials) to assemble the workshop modules that your customer is interested in.

Generally speaking, you should expect the workshop to follow this format:

1. Introductions and overview of Secure capabilities that will be covered (15-20 minutes)
1. Delivery of a module (5-10 minutes)
1. Q&A for the delivered module (5-10 minutes)
1. _Repeat the previous two steps for each planned module_
1. Review and final Q&A (10-15 minutes)

For planning and scheduling purposes, it's safe to aim for the upper end of each estimate. So, if you're only delivering a single module, you could potentially complete the workshop in 60 minutes. However, we'll likely be delivering more than one module, so it's best to plan for a minimum of 90 minutes to allow for extra questions and not feel rushed.

### Team Support

It is **highly recommended** that you have at least one other team member helping you to deliver the workshop. This person can monitor the chat (or other designated locations) for questions and provide answers, and moderate the chat.

### Attendee Registration

The recommended way to manage attendees is by setting up a [self-service virtual event](/handbook/marketing/revenue-marketing/field-marketing/#self-service-virtual-event-with-or-without-promotion). This will allow participants to register themselves, and will provide you with email addresses for attendees for follow-ups.

If your customer would prefer not to use this method, you can suggest that they provide you a list of attendees that you can subsequently send out a meeting invite to.

### Pre-Workshop Review

Set up a planning review meeting in advance of the workshop delivery date with the customer as well as any GitLab team members that will be part of delivering the workshop to go over what you have assembled, and how the workshop will be conducted. This should include:

- The agenda for the workshop, factoring in the time for each module as well as Q&A
- Overview of the materials, such as slide decks and references
- Identification of which (if any) modules will be pre-recorded
- How Q&A will be conducted (live chat, Google doc, periodic live Q&A breaks, etc.)
- Introduction of the team members that will be supporting the workshop delivery

## Workshop Materials

The materials for this workshop are intended to be delivered based on the customer's expressed interest in Secure capabilities. Depending on your level of familiarity with the Secure stage capabilities, you (and your [supporting team members](#team-support)) can conduct the session directly, or utilize available pre-recorded videos.

The slide decks provided contain information for the entire Secure stage. You can make a copy of the deck(s) you need for your workshop, and tailor the content to the features you'll be covering with the customer.

### Slide Decks

- [Secure Capabilities - Click-through demo](https://docs.google.com/presentation/d/11sr10_CZi93b5Um24GY2oXZUB3CWr-moGzft1YZMfOQ/edit?usp=sharing) _This provides the slides needed to deliver the workshop directly, without using the pre-recorded videos. This deck will be broken out into feature-based modules in a future iteration._
- [GitLab Security & Compliance Capabilities](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing) _this deck will likely be best used as an initial overview of Secure, either as part of selling the workshop or as a basic introduction at the beginning of the workshop_.

### Videos

- [Shifting Security Left - GitLab DevSecOps Overview](https://www.youtube.com/watch?v=XnYstHObqlA)
- [Adding Security to your GitLab CI/CD Pipeline](https://www.youtube.com/watch?v=Fd5DhebtScg)

#### Videos by Feature

- [SAST](https://www.youtube.com/watch?v=8sOjvlkl8QY)
- [DAST](https://www.youtube.com/watch?v=9tIrrByOum4)
- [Dependency Scanning](https://www.youtube.com/watch?v=39RvTMLDszc)
- [License Compliance](https://www.youtube.com/watch?v=42f9LiP5J_4)
- [Container Scanning](https://www.youtube.com/watch?v=wIcaSerMfFQ)
