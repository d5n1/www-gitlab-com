---
layout: handbook-page-toc
title: Social Advocacy Curator Program
description: Strategies and details to enable curators to share GitLab-related news for company-wide enablement
twitter_image: /images/opengraph/handbook/social-marketing/social-handbook-top.png
twitter_image_alt: GitLab's Social Media Handbook branded image
twitter_site: gitlab
twitter_creator: gitlab
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ⚠️ `THIS HANDBOOK PAGE IS UNDER CONSTRUCTION AND IS NOT CONSIDERED TO BE LIVE ADVICE AT THIS TIME` Please reach out to #social_media_action Slack channel with any questions

Current list of curators `tbd this list is pending`

| Name | Topic |
| ------ | ------ | 
| Jennifer Leslie | Awards and contributed articles |
| Jessica Reeder | All Remote |
| Nuritzi Sanchez | Open Source |
| Heather Simpson | Security | 
| Kira Aubrey | Field Marketing / PubSec |
| Madison Taft | Sales | 
| Ash Withers | Brand campaigns | 
| Sara Kassabian | Blogs (may cross into other topics as needed) | 
| cell | cell | 
| cell | cell | 
| cell | cell | 
