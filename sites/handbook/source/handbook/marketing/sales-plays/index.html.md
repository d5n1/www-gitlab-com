---
layout: handbook-page-toc
title: "Sales Plays"
description: "Sales plays are designed to improve the quantity of opportunities and closed business with defined Land and Expand sales motions"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Why Sales Plays

Sales plays are designed to improve the quantity of opportunities and closed business with defined Land and Expand sales motions to ultimately generate more ARR for GitLab. More specifically, codified sales plays help to ensure that the sales team is trained and capable of executing either (or both) of the following:
1. A repeatable set of proven successful sales tactics (Evergreen sales plays) 
1. A generated set of steps in coordination with a particular time-bound campaign (Coordinated sales plays)

## Sales Play Goals

Specific sales play goals may vary from quarter to quarter but may include (but are not necessarily limited to) the following:
- Increase the number of sales-generated First Order opportunities
- Improve win rates against a specific competitor 
- Increase MQL to SAO conversion 

## What is a Sales Play


## Types of Sales Plays


## Sales Play Bill of Materials


## Sales Plays

### Sales Play 1


### Sales Play 2


### Sales Play 2
